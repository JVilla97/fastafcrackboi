#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char plainText[PASS_LEN];
    int plaintxtLength;
    char hash[HASH_LEN];
    int hashLength;
    int found;
};
int file_length(char *filename)
{
    struct stat info;
    int res = stat(filename, &info);
    if (res == -1) return -1;
    else return info.st_size;
}
int comp_alpha(const void *a, const void *b)
{
    struct entry *ca = (struct entry *)a;
    struct entry *cb = (struct entry *)b;
    //printf("1-%s 2-%s\n", ca->hash, cb->hash);
    return strcmp(ca->hash, cb->hash);
}
int bcompare(const void *target, const void *elem)
{
    char *target_str = (char *)target;
    struct entry *celem = (struct entry *)elem;
    //printf("1-%s 2-%s\n", target_str, celem->hash);
    return strcmp(target_str, celem->hash);
}
struct entry *read_dictionary(char *filename, int *size)
{
    int filelength = file_length(filename);
    FILE *d = fopen(filename, "r");

    if(!d)
    {
        printf("Can't open %s for reading \n", filename);
        exit(1);
    }
    char *words = malloc(filelength);
    char *wordsTemp = words;
    fread(words, 1, filelength, d);
    fclose(d);

    int lines = 0;
    for(int i = 0; i < filelength; i++)
    {
        if(words[i] == '\n')
        {
            words[i] = '\0';
            lines ++;
        }
    }
    printf("Lines %d\n", lines);

    struct entry *entries = malloc(lines * sizeof(struct entry));

    for(int i = 0; i < lines; i++)
    {
		strcpy(entries[i].plainText, words);
		entries[i].plaintxtLength = strlen(entries[i].plainText);
		words += strlen(words)+1;
        
		char *hash = md5(entries[i].plainText, strlen(entries[i].plainText));

        strcpy(entries[i].hash, hash);
        entries[i].hashLength = strlen(entries[i].hash);
        entries[i].found = 0;
        //printf("%s %s\n", entries[i].plainText, entries[i].hash);
    }
    free(wordsTemp);
    *size = lines;
    return entries;
}
int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Usage: %s hash_file dict_file %d\n", argv[1], argc);
        exit(1);
    }
    int dlen;
    char lines[HASH_LEN];
    //Read the dictionary file into an array of entry structures
    struct entry * dict = read_dictionary(argv[2], &dlen);
    qsort(dict, dlen, sizeof(struct entry), comp_alpha);
    // Open the hash file for reading.
    FILE *hash = fopen(argv[1], "r");
 
    while(fgets(lines, HASH_LEN, hash) != NULL)
    {
        // For each hash, search for it in the dictionary using binary search.
        //printf("--%s\n", lines);
        struct entry * found = bsearch(lines, dict, dlen, sizeof(struct entry), bcompare);
        if(found != NULL)
        {
            found->found = 1;
        }
    }
    for(int i = 0; i < dlen; i++)
    {
        if(dict[i].found == 1)
        {
          printf("Match: %s %s\n", dict[i].hash, dict[i].plainText);  
        }
    }
    for(int i = 0; i < dlen; i++)
    {
        if(dict[i].found == 0)
        {
          printf("-%s %s\n", dict[i].hash, dict[i].plainText);  
        }
    }
    free(dict);
    fclose(hash);
}
